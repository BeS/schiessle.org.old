<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<meta name="microid" content="mailto+http:sha1:b558840b6b12dfa4f534b367b51c7b9edc5c3ea2" />
<link rel="openid.server" href="http://openid.claimid.com/server" />
<link rel="openid.delegate" href="http://openid.claimid.com/schiesbn" />
<link href="https://plus.google.com/110440960198152941904" rel="publisher" />
<link rel="stylesheet" href="main.css" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="author" href="https://plus.google.com/110440960198152941904" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Björn Schießle's Personal Homepage</title>
</head>
  
<body>
  
<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

<p>
  You can find me on various social networks. But I strongly prefer free and
  distributed networks which respect our privacy. Still I also have account on
  other networks, mainly to claim my name and as a way to forward people to my
  homepage. I decided to group them into three categories. 
</p>

<h1>Main Social Networks</h1>

<p>
  Here I'm online regularly and interact with other people. This are free and
  decentralized social networks which respects our privacy. Social networking
  how I like it. Feel free to connect with me and get in contact.
</p>

<p>
  <a href="https://io.schiessle.org/bes" class="sn-button u-url">
    <img src="img/network/gnusocial.png"
         alt="Join me on GnuSocial"/>
    GNU Social
  </a>
</p>
<p>
  <a href="https://diasp.eu/u/bjoern" class="sn-button u-url">
    <img src="img/network/diaspora.png" alt="Join me on Diaspora"/>
    Diaspora
  </a>
</p>

<h1>Neutral Social Networks</h1>

<p>
  Here I'm online regularly and interact with other people. I prefer one of the
  networks listed above but if you only have a account on one of this networks
  feel free to connect with me and get in contact.
</p>

<p>
  <a href="https://twitter.com/schiessle" class="sn-button u-url">
    <img src="img/network/twitter.png" alt="Follow me on Twitter"/>
    Twitter
  </a>
</p>

<p>
  <a href="https://gitlab.com/u/BeS" class="sn-button u-url">
    <img src="img/network/gitlab.png" alt="My GitLab Projects"/>
    GitLab (hosting private stuff like this homepage)
  </a>

<p>
  <a href="https://github.com/schiessle" class="sn-button u-url">
    <img src="img/network/github.png" alt="My GitHub Projects"/>
    GitHub (mostly work related)
  </a>
</p>

<h1>Passive Social Networks</h1>

<p>
  I use this networks mostly as a passive online presence so that people can
  find me and get forwarded to my homepage. Please don't expect that I look at
  this networks regularly and response in time. Better use one of the contact
  addresses you can find on my homepage.
</p>

<p>
  <a href="https://plus.google.com/110440960198152941904" class="sn-button">
    <img src="img/network/google.png" alt="Google+" />
    Google+
  </a>
</p>
<p>
  <a href="https://linkedin.com/in/schiessle" class="sn-button">
    <img src="img/network/linkedin.png" alt="Linked.In"/>
    LinkedIn
  </a>
</p>
<p>
  <a href="https://www.xing.com/profile/Bjoern_Schiessle" class="sn-button">
    <img src="img/network/xing.png" alt="Xing"/>
    Xing
  </a>
</p>
<p>
  <a href="https://www.facebook.com/bjoern.schiessle" class="sn-button">
    <img src="img/network/facebook.png" alt="Facebook" />
    Facebook
  </a>
</p>


</div>

<?php echo file_get_contents("footer.html") ?>

</body>
</html>
