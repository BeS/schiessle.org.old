<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="privacy.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Secure and private communication</title>
</head>
  
<body>

<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
  
<div id="content">

  <p class="quote">
    <i>
    <?php $random = rand(0, 9); 
          if ($random < 5) {
             echo '"If privacy is outlawed, only outlaws will have privacy."';
          } else {
             echo '"Privacy is a right like any other. You have to exercise it or risk losing it."';
          }
    ?>
    </i><br />
  ‒ <a href="https://en.wikipedia.org/wiki/Phil_Zimmermann">Phil Zimmermann</a>
</p>

  
<h1>Secure and private communication, <em>use encryption!</em></h1>

<p>
  The real world equivalent of an e-mail is a postcard. If we send a
  postcard we know that everyone who get in touch with it can read it,
  therefore often we chose to write a letter instead. By default, the
  same is true for e-mails and instance messages. It is important to
  keep this in mind. Especially because this messages pass through many
  servers before they arrive at the inbox of the recipient. This means
  that many people are able to copy and read our messages. Since Snowden we
  know that many providers and public authorities store and analyse
  this messages.
</p>

<p>
  There are tools which allows us to easily encrypt our messages. I
  highly recommend to look into this possibilities and use encryption
  whenever possible. On this page you find everything you need to send
  me secure and private messages.
</p>
  
<h2>Send me a private e-mail</h2>

  <p>
    If you want to send me a e-mail in a secure and private way you
    can use the GNU Privacy Guard (GnuPG). This is a tool available for
    almost all e-mail clients and even for some web mailer. Read
    this <a href="https://emailselfdefense.fsf.org/en/">tutorial</a>
    if you want to learn how to use it.
  </p>

  <p>
    My GnuPG Key: <a href="gpg-key.txt" class="keydata">0x2378A753E2BF04F6</a></i><br/>
    My GnuPG Fingerprint:  <span class="keydata">244F CEB0 CB09 9524 B21F B896 2378 A753 E2BF 04F6</span><br />
  </p>

  <p>
    For cross verification you can also find my GnuPG key
    at <a href="https://keybase.io/BeS">Keybase</a>.
  </p>

  <h2>Let's chat in a secure and private way</h2>

  <p>
    For Chat you can use the free, decentralized and federated
    protocol XMPP (also known as Jabber). For encryption I recommend
    the <a href="https://conversations.im/omemo/">OMEMO
    protocol</a>. OMEMO creates for each device a seperate key. You
    can find here all my device specific keys.
  </p>

  <p class="fingerprint">
    <img src="fingerprint1.png" alt="QR-Code for device 1"/>
    Fingerprint (Device 1)<br/><br/>
    <span class="keydata">
      ED76D244 5E36361A<br/>
      58AD002A 818EBA40<br/>
      C65721F7 5F15E547<br/>
      F86C2C03 6BFAD01A
    </span>
  </p>

  <p class="fingerprint">
    <img src="fingerprint2.png" alt="QR-Code for device 2"/>
    Fingerprint (Device 2)<br/><br/>
    <span class="keydata">
      4B777B3F 79C77DC4<br/>
      FFFD9529 A6F633B5<br/>
      E9BBC152 E7CFB3FF<br/>
      B6FA1397 4599CD6F
    </span>
  </p>

  <div class="clear"></div>

<p>
  If XMPP (Jabber) is completely new to you and you want to give it
  a try I recommend this tutorials
  for <a href="https://medium.com/@mathiasrenner/setup-whatsapp-like-chat-messaging-with-open-source-software-complete-guide-ec7adc0d3519#.ftkcos8ne">mobile
  devices</a> and
  <a href="https://medium.com/@mathiasrenner/setup-xmpp-with-omemo-encryption-on-your-desktop-7f6accd8dc16#.essat77ud">desktop
  clients</a>.
  </p>
  
</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</body>
</html>
