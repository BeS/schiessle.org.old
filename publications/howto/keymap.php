<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="../main.css" type="text/css" />
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<title>Björn Schießle - us_de Keymap</title>
</head>
  
<body>

<div id="page">

<div id="menu">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

<h1>Amerikanische Tastatur mit deutschen Umlaute</h1>
    
<p>
Für alle die eine amerikanische Tastatur haben und trotzdem nicht auf die
deutschen Umlaute verzichten wollen gibt es hier eine angepasste
Tastaturbelegung für X11 und die Konsole.
</p>
<p>
Folgende Zeichen werden mit der neuen Tastaturbelegung ermöglicht:
</p>

<table class="center" border="1" cellpadding="10">
<colgroup>
<col width="1*" />
<col width="2*" />
<col width="2*" />
</colgroup>

<tr>
<th>Zeichen</th>
<th>Konsole</th>
<th>X11</th>
</tr>

<tr>
<td align="center">ä</td>
<td colspan="2" align="center">[Alt_R] + [a]</td>
</tr>

<tr>
<td align="center">Ä</td>
<td colspan="2" align="center">[Alt_R] + [A]</td>
</tr>

<tr>
<td align="center">ö</td>
<td colspan="2" align="center">[Alt_R] + [o]</td>
</tr>

<tr>
<td align="center">Ö</td>
<td colspan="2" align="center">[Alt_R] + [O]</td>
</tr>

<tr>
<td align="center">ü</td>
<td colspan="2" align="center">[Alt_R] + [u]</td>
</tr>

<tr>
<td align="center">Ü</td>
<td colspan="2" align="center">[Alt_R] + [U]</td>
</tr>

<tr>
<td align="center">ß</td>
<td colspan="2" align="center">[Alt_R] + [s]</td>
</tr>

<tr>
<td align="center">Euro-Symbol</td>
<td align="center">-</td>
<td align="center">[Alt_R] + [e] und [Alt_R] + [5]</td>
</tr>

</table>

<p>
Die Beschreibung bezieht sich auf Debian GNU/Linux Systeme. Die neue
Tastaturbelegung sollten aber auch mit jeder anderen GNU/Linux Distribution
funktionieren, lediglich die Verzeichnisse könnten sich unterscheiden.
</p>

<h2>Konsole:</h2>

<p>
Hierzu müssen Sie die /etc/console/boottime.kmap.gz durch die neue  
<a href="../data/keymap/boottime.kmap.gz">boottime.kmap.gz</a> ersetzen und
die neue Tastaturbelegung laden:
</p>
<pre>/etc/init.d/keymap.sh restart</pre>
<!--
<h2>X11:</h2>
<p>
Natürlich muß die Konfigurationsdatei angepasst werden:
<br/><br/>
XFree (/etc/X11/XF86Config-4):
</p>
<pre>
Section "InputDevice"
        Identifier      "Generic Keyboard"
        Driver          "keyboard"
        Option          "CoreKeyboard"
        Option          "XkbRules"      "xfree86"
        Option          "XkbModel"      "pc104"
        Option          "XkbLayout"     "us_de"
EndSection
</pre>
-->

<h2>X.org</h2>

<p>
/etc/X11/xorg.conf:
</p>
<pre>
Section "InputDevice"
        Identifier      "Generic Keyboard"
        Driver          "kbd"
        Option          "CoreKeyboard"
        Option          "XkbRules"      "xorg"
        Option          "XkbModel"      "pc104"
        Option          "XkbLayout"     "us_de"
EndSection
</pre>


<p>
Jetzt müssen Sie nur noch die neue Tastaturbelegung in das entsprechende
Verzeichnis kopieren:
<br />
<!--
Für XFree 4.2 kopieren Sie 
<a href="../data/keymap/xfree4.2/us_de">us_de (XFree 4.2)</a> 
nach /etc/X11/xkb/symbols.
<br />
Für XFree 4.3 kopieren Sie
<a href="../data/keymap/xfree4.3/us_de">us_de (XFree 4.3)</a>
nach /etc/X11/xkb/symbols/pc.  
<br /> -->
Dafür kopieren Sie diese Datei
<a href="../data/keymap/xorg/us_de">us_de (X.org)</a>
nach /usr/share/X11/xkb/symbols.
</p>
</div>

<?php echo file_get_contents("/home/schiesbn/websites/schiessle.org/htdocs/footer-cc-by-sa.html") ?>

</div>

</body>
</html>
