<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="../main.css" type="text/css" />
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<title>Björn Schießle - Thinkpad X24</title>
</head>
  
<body>

<div id="page">

<div id="menu">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

<p class="right">
<img src="/img/thinkpad_x24.png" alt="IBM Thinkpad X24" />
</p>

<h1>Debian GNU/Linux auf einem IBM Thinkpad X24</h1>

<p>
Installiertes System: 
<b>Debian GNU/Linux 3.0 (woody) / Kernel 2.4.19 / XFree 4.1</b>
</p>
<p>
letztes Update: 04.11.2003
</p>
      
    
<h2>Hardware</h2>

<p>
<a
href="http://www5.pc.ibm.com/de/products.nsf/$wwwPartNumLookup/_TX4MWGE?OpenDocument">IBM
Thinkpad X24</a> (Modellbezeichnung: 2662MWG)
</p>
 
<ul>
<li> Mobile Intel Pentium III Prozessor, 1133 MHz, 133 MHz Front Side Bus,
      256 KB Level 2 Cache</li>
<li> 256MB SDRAM</li>
<li> 30,0 GB ATA-100</li>
<li> 12,1'' TFT-Farbdisplay, 1.024x768x16,7 Millionen Farben</li>
<li> ATI Mobility Radeon M6 8 MB SDRAM</li>
<li> Soundchip ES1988 (Allegro1)</li>
<li> Integriertes internationales V.90 Modem (56 Kbps Daten, 14,4 Kbps
      Fax)</li>
<li> 10/100 Ethernetadapter Intel Pro/100 VE Network Connection PCI</li>
<li> 802.11b Wireless-Lanadapter 11 MBit/s</li>
</ul>

<pre>
00:00.0 Host bridge: Intel Corp.: Unknown device 3575 (rev 04)
00:01.0 PCI bridge: Intel Corp.: Unknown device 3576 (rev 04)
00:1d.0 USB Controller: Intel Corp.: Unknown device 2482 (rev 02)
00:1d.1 USB Controller: Intel Corp.: Unknown device 2484 (rev 02)
00:1d.2 USB Controller: Intel Corp.: Unknown device 2487 (rev 02)
00:1e.0 PCI bridge: Intel Corp. 82820 820 (Camino 2) Chipset PCI (-M) (rev 42)
00:1f.0 ISA bridge: Intel Corp.: Unknown device 248c (rev 02)
00:1f.1 IDE interface: Intel Corp.: Unknown device 248a (rev 02)
00:1f.3 SMBus: Intel Corp.: Unknown device 2483 (rev 02)
00:1f.5 Multimedia audio controller: Intel Corp. AC'97 Audio Controller (rev 02)
00:1f.6 Modem: Intel Corp.: Unknown device 2486 (rev 02)
01:00.0 VGA compatible controller: ATI Technologies Inc Radeon Mobility M6 LY
02:03.0 CardBus bridge: Ricoh Co Ltd RL5c476 II (rev 80)
02:03.1 CardBus bridge: Ricoh Co Ltd RL5c476 II (rev 80)
02:05.0 Network controller: Harris Semiconductor: Unknown device 3873 (rev 01)
02:08.0 Ethernet controller: Intel Corp. 82801CAM (ICH3) Chipset Ethernet Controller (rev 42)
</pre>

<p>
<a href="../data/x24/thinkpad_details.html">
Hier gibt es die komplette Ausgabe von 'lspci -vx'</a>
</p>   

    
<h2>Installation</h2>

<p>
Wenn Sie ein Media Slice mit CD/DVD-Laufwerk besitzen sollte die
Installation problemlos funktionieren.
</p>

<p>
Falls Sie aber lediglich über ein USB-Diskettenlaufwerk verfügen brauchen
sie spezielle Disketten, da die normalen Installationsdisketten Probleme
mit USB Laufwerken haben. Die Disketten finden Sie hier:
</p>
 
<ul>
<li><a href="../data/x24/resc.bin">
resc.bin</a></li>
<li><a href="../data/x24/root.bin">
root.bin</a></li>
<li><a href="../data/x24/driver-1.bin">
driver-1.bin</a></li>
<li><a href="../data/x24/driver-2.bin">
driver-2.bin</a></li>    
<li><a href="../data/x24/driver-3.bin">
driver-3.bin</a></li>    
<li><a href="../data/x24/driver-4.bin">
driver-4.bin</a></li>    
<li><a href="../data/x24/driver-5.bin">
driver-5.bin</a></li>  
</ul>

<p>
Die Disketten könne Sie mit diesem Befehl erstellen:
</p>
<pre>
      dd if=&#60;file&#62; of=/dev/fd0 bs=1024 conv=sync ; sync
</pre>

<p>
Danach booten Sie das System mit der rescue-Diskette und folgen den
Anweisungen. Nachdem die root-Diskette geladen ist wechseln Sie in die
Console und legen einen Link für /dev/sda nach /dev/fd0 an (ln -s /dev/sda
/dev/fd0). Danach können Sie wieder zurück zur Installationsroutine
wechseln und die Installation normal durchführen.
</p>
<p>
Für weitere Informationen zur Netzwerksinstallation lesen Sie
<a href="http://www.debian.org/distrib/floppyinst">diese
Anleitung</a>.
</p>
 
<h2>Netzwerk-Interface</h2>
<p>
Um die Ethernet Netzwerkkarte zu betreiben müssen Sie das Modul <i>eepro100</i>
laden.
</p>
<p>
Um die wlan Karte benutzen zu können brauchen Sie die Treiber von
<a href="http://www.linux-wlan.org/">linux-wlan.org</a> und die
kernel-sources des verwendeten Kernels.
</p>
<p>
Die Kernel-Quellen sollten in /usr/src/linux liegen.
</p>
<p>
Jetzt müssen Sie die wlan Treiber in /usr/src entpacken, danach können Sie
in das Verzeichnis /usr/src/linux-wlan-ng-x.y.z wechseln und die Treiber
installieren:
</p>
<pre>
     make config
     make all
     make install
</pre>

<p>
Während <code>make config</code> werden Sie gefragt welche module erstellt
werden sollen.
</p>
<p>
Bei <code>Build Prism2.5 native PCI (_pci) driver? (y/n) [n]:</code> müssen
Sie mit <code>y</code> antworten.
</p>
<p>
Das ist das einzige Modul das Sie für die wlan Karte brauchen, alle anderen
Fragen können also mit <code>n</code> beantwortet werden.
</p>
<p>
Nach der erfolgreichen Installation finden Sie das Modul prism2_pci.o in
/lib/modules/&lt;kernel_version&gt;/net
</p>
<p>
Danach müssen Sie noch in /etc/modules.conf diese Zeile einfügen:
</p>
<pre>
     alias wlan0 prism2_pci
</pre>

<p>
Und mit dem Eintrag von <code>prism2_pci</code> in /etc/modules dafür
sorgen, dass der Treiber beim booten auch geladen wird.
</p>
<p>
Jetzt brauchen Sie nur noch ein kleines Script, welches die
wlan-Karte konfiguriert:
</p>
<pre>
     #!/bin/sh

     echo 'Starting wlan interface...'

     wlanctl-ng wlan0 lnxreq_ifstate ifstate=enable
     wlanctl-ng wlan0 lnxreq_autojoin ssid=&lt;your_APs_SSID&gt; authtype=opensystem

     #WEP Konfiguration (optional, aber empfohlen)
     # Default-Key auswählen
     wlanctl-ng wlan0  dot11req_mibset  mibattribute=dot11WEPDefaultKeyID=0
     # Unverschlüsslete Teilnehmer nicht akzeptieren 
     wlanctl-ng wlan0  dot11req_mibset  mibattribute=dot11ExcludeUnencrypted=true
     # WEP anschalten 
     wlanctl-ng wlan0  dot11req_mibset  mibattribute=dot11PrivacyInvoked=true
     # WEP-Key setzen (hier WEP128 Beispiel)
     wlanctl-ng wlan0  dot11req_mibset  mibattribute=dot11WEPDefaultKey0=01:02:03:04:05:06:07:08:09:10:11:12:13

     # Netzwerkkarte aktivieren und den Gateway setzen
     ifconfig wlan0 &lt;your_IP&gt; netmask &lt;your_netmask&gt; broadcast &lt;your_broadcast&gt;
     route add default gw &lt;your_gateway&gt;
</pre>

<h2>Modem</h2>
<p>
Das integrierte V.90 Modem ist ein WinModem, womit die Chancen ziemlich
gering sind das man es zum laufen bekommt. Sollten Sie trotzdem versuchen
das Modem in Betrieb zu nehmen könnte ein Blick auf 
<a href="http://www.linmodems.org">linmodems.org</a> hilfreich
sein.
</p>

<h2>Sound</h2>
<p>
Um die Soundkarte in Betrieb zu nehmen sollte das laden des Muduls
<i>i810_audio</i> ausreichen.
</p>
<p>
Hinweis: Vergessen Sie nicht sich der Gruppe audio hinzuzufügen, damit Sie 
auch als normaler user auf die Soundkarte zugreifen können.
</p> 

<h2>Grafik:</h2>
<p>
Die ATI Mobility Radeon M6 können Sie mit der XFree Version aus Debian
woody nur im framebuffer Modus mit dem vesa Treiber nutzen.
</p>
<p>
Wenn Sie die radeon Treiber verwenden wollen, was auch zu empfehlen ist,
brauchen Sie eine neuere Version von XFree welche sie aus testing
installieren können.
</p>
<p>
Wenn Sie ihr System lieber auf stable/woody belassen wollen gibt es auch
einen Backport von XFree4.2. Diesen können Sie aus dieser Quellen
installieren, die Sie in ihre /etc/apt/sources.list eintragen müssen.
</p> 
<pre>
    deb http://people.debian.org/~frankie/debian woody/x421/
</pre>
<p>
X lässt sich sowohl mit dem TrackPoint als auch mit einer angeschlossenen
USB-Maus steuern.
</p>
<p>
Eine entsprechende XF86Config-4 (für die radeon Treiber) finden Sie
<a href="../data/x24/XF86Config-4">hier</a>
</p> 

<h2>IRDA</h2>
<p>
Leider kann ich hierzu nicht viel sagen, da ich bisher noch nicht in der
Lage war die Infrarot-Schnittstelle zu testen. IRDA sollte aber in aller
Regel keine Probleme bereiten.
</p>
<p>
Für Informationen hierzu können Sie sich vergleichbare Notebooks aus der X
Serie auf <a href="http://www.tuxmobil.org">tuxmobil.org</a> oder 
<a href="http://www.linux-laptop.net">linux-laptop.net</a>
ansehen.
</p>

<h2>APM</h2>
<p>
APM sollte mit Debian GNU/Linux problemlos funktionieren, ich habe dazu
Folgende Kernel-Optionen aktiviert:
</p>
<pre>
      CONFIG_PM=y
      CONFIG_ACPI=y
      # CONFIG_ACPI_DEBUG is not set
      CONFIG_ACPI_BUSMGR=m
      CONFIG_ACPI_SYS=m
      CONFIG_ACPI_CPU=m
      CONFIG_ACPI_BUTTON=m
      CONFIG_ACPI_AC=m
      CONFIG_ACPI_EC=m
      CONFIG_ACPI_CMBATT=m
      CONFIG_ACPI_THERMAL=m
      CONFIG_APM=y
      # CONFIG_APM_IGNORE_USER_SUSPEND is not set
      CONFIG_APM_DO_ENABLE=y
      CONFIG_APM_CPU_IDLE=y
      # CONFIG_APM_DISPLAY_BLANK is not set
      # CONFIG_APM_RTC_IS_GMT is not set
      CONFIG_APM_ALLOW_INTS=y
      CONFIG_APM_REAL_MODE_POWER_OFF=y
</pre>

<h2>Kernel</h2>
<p>
zum Schluss habe ich
<a href="../data/x24/config-2.4.19">hier</a> noch eine komplette
Kernel Konfiguration für Linux 2.4.19.
</p>
</div>

<?php echo file_get_contents("/home/schiesbn/websites/schiessle.org/htdocs/footer-cc-by-sa.html") ?>

</div>

</body>
</html>
