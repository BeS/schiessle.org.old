<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="../main.css" type="text/css" />
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<title>Björn Schießle - GnuPG Smartcard and Poldi</title>
</head>
  
<body>

<div id="page">

<div id="menu">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

<h1>Login with a GnuPG Smartcard and Poldi</h1>
    
<p>Libpam-poldi allows you to use a GnuPG smartcard to log in your
GNU/Linux system.</p>

<h2>Install libpam-poldi</h2>

<p>
If you use Debian GNU/Linux libpam-poldi is already available in the
repositories.<br />

On Ubuntu GNU/Linux you can't use the Debian binaries but you can download the
sources of libpam-poldi from Debian and than build a package for your Ubuntu
system with 'dpkg-buildpackage -us -uc'.</p>


<h2>Configure Poldi</h2>

<p>First check if poldi detects your cardreader: 'poldi-ctrl -d'. Unfortunately
some cardreader doesn’t work with poldi and the existing free driver. For
example the cardma4040 needs the non-free driver from Omnikey.</p>

<p>If poldi successfully detected your cardreader you can start to configure
poldi. Poldi has a pretty good documentation so i will keep my explanations
rather short.</p>

<ol>
<li>Root has to register the new card for poldi:
<pre>poldi-ctrl --register-card --account &lt;your-user-account&gt; --serialno &lt;serialno of your card&gt;</pre>
You can also execute this command without '--account &lt;your-user-account&gt;'
 but than the user want be able to install or update his card's keys.<br />
The serialno can be found by executing 'gpg --card-status' and looking for 
"Application ID".
</li><br />
<li>Now we have to establish a mapping between the user and the smartcard he
owns:
<pre>poldi-ctrl --associate --account &lt;your-user-account&gt; --serialno &lt;serialno of your card&gt;</pre></li>
<li>Now you have to write your public key into the appropriate key file (you
have to do this within your user account)
<pre> poldi-ctrl --set-key</pre></li>
<li>That's it, now you can test it with 'poldi-ctrl --test'</li>
</ol>

<h2>Configure Pam</h2>
<p>
Now you have to tell pam, that you want to use poldi.<br />
Therefore you have to edit the files in /etc/pam.d.
If you want to use your smartcard with gdm than...
</p>
<ol>
<li>make a backup of /etc/pam.d/gdm.
<pre>mv /etc/pam.d/gdm /etc/pam.d/gdm.default</pre></li>
<li>create a new /etc/pam.d/gdm which contains only one of these lines:  
<pre>auth    required   pam_poldi.so quiet</pre>
<p>Or if you want to login unattended, use</p>
<pre>auth    required   pam_poldi.so try-pin=123456 quiet</pre>
<p>Or if you want to fallback to regular unix passwords, use</p>
<pre>auth    sufficient pam_poldi.so try-pin=123456 quiet
auth    required   pam_unix.so nullok_secure</pre>
</li>
</ol>

<p>Now you should be able to use your GnuPG smartcard to log in your GNU/Linux
system.</p>
</div>

<?php echo file_get_contents("/home/schiesbn/websites/schiessle.org/htdocs/footer-cc-by-sa.html") ?>

</div>

</body>
</html>
