<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all">
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Björn Schießle - Publications</title>
</head>
  
<body>

<div id="page">

<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

<p class="quote">
  <i>"Omnis enim res, quae dando non deficit, dum habetur et non datur, nondum
    habetur, quomodo habenda est."</i><br />
  ‒ <a href="/augustinus">Aurelius Augustinus</a>
</p>

<h1>Scientific publications</h1>

<ul>
<li>Freie Software – Ein wachsender Faktor in Wirtschaft und
  Gesellschaft. Erschienen im Handbuch der Unternehmensberatung, 2013, Band 2, Kennzahl 3410, Erich Schmidt Verlag</li>

<li>Creating and using roboearth object models. CRA, pages 3549–3550,
  2012. Daniel Di Marco, Andreas Koch, Oliver Zweigle, Kai Häussermann, Björn
  Schiessle, Paul Levi, Dorian Gálvez-López, Luis Riazuelo, Javier Civera,
  J. M. M. Montiel, Moritz Tenorth, Alexander Clifford Perzylo, Markus Waibel,
  and René van de Molengraft
</li>
<li>Server-sided automatic map transformation in roboearth. AMS, pages 203–216,
  2012. Alexander Clifford Perzylo, Björn Schießle, Kai Häussermann, Oliver
  Zweigle, Paul Levi, and Alois Knoll.
</li>
</li>
<li>RoboEarth - A World Wide Web for Robots. IEEE Robotics and Automation Magazine (Special Issue Towards a WWW for Robots), 2011. Accepted - Waibel, M., Beetz, M., D’Andrea, R., Janssen, R., Tenorth, M., Civera, J., Elfring, J., Galvez-Lopez, D., Häussermann, K., Montiel, J.M.M., Perzylo, A., Schießle, B., Zweigle, O., and van de Molengraft, R. (2011)
</li>
<li>Using Situation Analysis Techniques with Bayesian Networks for Recognizing Hardware Errors on Mobile Robots - Oliver Zweigle, Kai Häussermann, Björn Schiessle and Paul Levi (ICRA 2011) </li>
</ul>

<h1>Education Resources</h1>

<ul>
<li><a href="http://www.medien-in-die-schule.de/werkzeugkaesten/werkzeugkasten-freie-software/">Werkzeugkasten Freie Software</a> - Einführung in die Grundlagen Freier Software zusammen mit einer Sammlung von sinnvoller Software, mit der Unterricht in einem digitalen Zeitalter gestaltet werden kann. Autoren: Volker Grassmuck, Daniel Rohde-Kage, Björn Schießle, Stefan Schober, Sebastian Seitz, Wolf-Dieter Zimmermann (2016) 
</li>
</ul>

<h1>Online articles</h1>

<ul>
  <li>
    <a href="https://netzpolitik.org/2017/kommentar-die-abkehr-von-freier-und-unabhaengiger-software-in-muenchen-waere-falsch/">Die Abkehr von freier und unabhängiger Software in München wäre falsch</a> (Kommentar auf Netzpolitik.org)
  </li>
  <li>
    <a href="https://netzpolitik.org/2016/software-heritage-erhalt-eines-kulturerbes/">Software Heritage – Erhalt eines Kulturerbes</a> (Gastbeitrag auf Netzpolitik.org)
  </li>
  <li>
    <a href="https://netzpolitik.org/2015/10-jahre-sony-rootkit-ein-fragwuerdiges-jubilaeum/">10
    Jahre Sony Rootkit – ein fragwürdiges Jubiläum</a> (Gastbeitrag
    auf Netzpolitik.org)
  </li>
  <li>
    <a href="https://netzpolitik.org/2015/user-data-manifesto-2-0/">User Data
      Manifesto 2.0</a> (Gastbeitrag auf Netzpolitik.org)
  </li>
  <li>
    <a href="https://fsfe.org/freesoftware/basics/comparison">Free Software,
      Open Source, FOSS, FLOSS – Same same but different</a>
  </li>
</ul>


<h1>During computer science studies (German)</h1>

<p>These documents have been created during my computer science studies at
  <a href="http://www.uni-stuttgart.de/index.en.html">University of
  Stuttgart</a>.</p>

<ul>
<li>Betriebssystemsicherheit: GNU/Linux (SELinux)<br />
(<a href="data/linuxsicherheit.pdf">PDF</a>)
(<a href="data/linuxsicherheit.tar.gz">LaTeX Sources</a>)</li>

<li>Peer-to-Peer Systeme: Content Addressable Networks (CAN)<br />
<blockquote><i>Zusammenfassung:</i> Die Algorithmen die zum auffinden von Daten
in einem Peer-to-Peer Systems verwendet werden, sind für das effiziente
Arbeiten eines solchen Systems sehr entscheidend. Mit Hashtabellen kennen wir
bereits eine effiziente Möglichkeit Daten zu finden, indem jedem Datum ein
Schlüssel zugeordnet wird. Mit verteilten Hashtabellen lässt sich dieses
Konzept auf verteilte Strukturen und Peer-to-Peer-Systeme übertragen. Content
Addressable Networks (CAN) sind eine mögliche Umsetzung dieser Idee. Im
folgenden soll ein konkretes CAN Design vorgestellt und Erweiterungen zur
Optimierung des Routing diskutiert werden.</blockquote>
(<a href="data/can_report.pdf">Report</a>, <a href="data/can_presentation.pdf">Presentation</a>)
(<a href="data/can.tar.gz">LaTeX Sources</a>)
</li>

</ul>

<p>
License: <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative
Commons Attribution-Share Alike 3.0 License</a>.
</p>


<h1>Miscellaneous</h1>

<ul>
<li><a href="howto/thinkpadx24.php">Debian GNU/Linux auf einem IBM Thinkpad X24</a> (German)</li>
<li>Cyrus-Imap Mailserver Howto (German) 
(<a href="howto/debian-imap-howto.pdf">PDF</a>)
</li>
<li><a href="howto/keymap.php">us_de keymap (deutsche Umlaute auf
US-Tastatur)</a> (German)</li>
<li><a href="howto/poldi.php">Login with a GnuPG smartcard and Poldi</a></li>
</ul>

<p class="right">
<a href="https://pdfreaders.org/">
<img src="img/pdfreaders.png"
   alt="pdfreaders.org" /></a>
</p>

</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</div>

</body>
</html>
