<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Send me attachments I can read, use Open Standards!</title>
</head>
  
<body>

<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
  
<div id="content">

<h1>Send me attachments I can read, <em>use Open Standards!</em></h1>

<p class="right">Originally published
  by <a href="http://hugoroy.eu/open-standards-att.php">Hugo Roy</a> 
  under the terms of <br /> 
  <a href="http://creativecommons.org/licenses/by-sa/2.0/fr/deed.en">Creative
  Commons - Attribution-Share Alike 2.0 France</a></p>

<p>There are many ways to share documents, files and data over the
  Internet. Among them, emails are often used because people can communicate
  from one mail server<a href="#mserv" id="ref-mserv" class="fnlink">1</a> to
  another without any difficulty. Why does it work so simply?
  Because <strong>emails are designed to use a set of Open
  Standards</strong><a href="#ostd" id="ref-ostd" class="fnlink">2</a>, based on
  the Internet protocols.</p>

<p>However, sometimes people send attachments along with their emails, and it
  happens frequently that the attachments cannot be read by the recipients. For
  example, many attached files are documents produced by word processors and it
  can be impossible to read them correctly if you do not have the same word
  processor. Many proprietary word processors use proprietary file
  formats<a href="#dotdoc" id="ref-dotdoc" class="fnlink">3</a>. In 2002, a
  campaign was started to <a 
  href="http://www.gnu.org/philosophy/no-word-attachments.html">put an end to
  Word attachments</a>. But the same is true for all kinds of documents and
  files: texts, spreadsheets, slides, videos, etc.</p>

<p>When you attach a file to an email, please make sure that your correspondent
  will be able to read your files correctly. It is a basic principle of
  courtesy. And there is an easy way to make this possible: use Open
  Standards. If you do so, your correspondent will have the possibility to
  choose which program he or she wants. <strong>Open Standards guarantee
  sustainability and interoperability</strong> for your data, making sure you
  will be able to access them in the future, even with another software, on
  another platform or operating system.</p>

<p>Moreover, by promoting Open Standards you will help
  everyone: <strong>sharing documents can be as easy as sending and receiving
  emails!</strong> If you receive an email using proprietary file formats,
  don't hesitate to share this page and explain why it is important to
  use <a href="http://www.fsfe.org/projects/os/">Open Standards</a>.</p>

<h2>Campaigns for Open Standards</h2>

<ul>
  <li><a href="http://www.documentfreedom.org/">Document Freedom Day</a></li>
  <li><a href="http://playogg.com">Play Ogg</a>
  and <a href="http://rogg-on.org/">rOgg On!</a></li>
  <li><a href="http://www.pdfreaders.org">PDFreaders.org</a></li>
</ul>

<p>On <a href="http://documentfreedom.org">Document Freedom Day</a>, the FSF
  started a campaign to call on computer users to start
  politely <a href="http://www.fsf.org/news/why-im-rejecting-your-email-attachment">rejecting
  proprietary attachments</a>.</p>

<h2>Organisations and Software supporting Open Standards</h2>

<ul>
  <li><a href="http://www.documentfoundation.org/">The Document Foundation</a></li>
  <li><a href="http://openoffice.org">OpenOffice.org</a></li>
  <li><a href="http://videolan.org">VideoLan</a>, the project making VLC</li>
  <li><a href="http://fsfe.org/projects/os/">Free Software Foundation Europe</a></li>
</ul>

<div class="footnotes">
<ol>
  <li id="mserv"><a href="#ref-mserv">^ </a> Mail servers transfer emails from
  one computer to another. For more information, you
  can <a href="http://en.wikipedia.org/wiki/Mail_server">check
  Wikipedia</a></li>
  <li id="ostd"><a href="#ref-ostd">^ </a> Open Standards are protocols and
  file formats that can be <strong>freely used and implemented</strong>, and
  are designed in an open process. For more information, see the
  <a 
  href="http://www.fsfe.org/projects/os/def.html"><strong>Open Standards
  definition</strong></a>.</li>
  <li id="dotdoc"><a href="#ref-dotdoc">^ </a> Microsoft Office by default save
  your files in the ".doc" format or the ".docx" format. If you share these
  documents with people using different word processors, it will not work
  properly. Good alternatives for Microsoft Word are documents in .RTF or in
  .ODT (use the "Save As" feature)</li>
</ol>
</div>

</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</body>
</html>
