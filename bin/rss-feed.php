<?php

//max number of rss items (0 == all items)
$max = 5;
// location of the stored feed file, needs to end with a '/' (empty == use script location)
$location = dirname(dirname(__FILE__)) . '/feeds/';

$feeds = array(
	"blog" => "http://blog.schiessle.org/feed/",
	"gnusocial" => "http://io.schiessle.org/api/statuses/user_timeline/1.rss",
);

$exclude = array('Björn Schießle</a> started following',
	'Björn Schießle</a> joined the group',
	'Björn Schießle</a> deleted notice',
	'Björn Schießle deleted notice', // work around for markdown plug-in
	'@',
	'bes favorited'
);

foreach ($feeds as $id => $url ) {
	if ( ($feed = simplexml_load_file($url)) == FALSE) continue;
	$path = $location.$id.'.html';
	$count = 1;

	// take twice as much GNU Social items than blog posts
	if ( $id == "gnusocial" ) $count = $count - $max + 3;

	$output = "<ul>\n";

	foreach ($feed->channel->item as $f) {
		if ( $id != 'gnusocial' ) {
			$output .= "<li>\n";
			$output .= "<a href=\"".$f->link."\">".$f->title."</a><br/>\n";
			$output .= "<div class=\"description\">".$f->description."</div>\n";
			$output .= "</li>\n";
			$count++;
		} else if ( $id == 'gnusocial' && strpos($f->description, $exclude[0]) !== FALSE) {
			continue;
		} else if ( $id == 'gnusocial' && strpos($f->description, $exclude[1]) !== FALSE) {
            continue;
		} else if ( $id == 'gnusocial' && strpos($f->description, $exclude[2]) !== FALSE) {
			continue;
		} else if ( $id == 'gnusocial' && strpos($f->description, $exclude[3]) !== FALSE) {
			continue;
		}else if ( $id == 'gnusocial' && strpos($f->description, $exclude[4]) === 0) {
			continue;
		}else if ( $id == 'gnusocial' && strpos($f->description, $exclude[5]) === 0) {
			continue;
		} else {
			$output .= "<li>\n";
			$output .= "<div class=\"description\">".$f->description."</div>\n";
			$output .= "</li>\n";
			$count++;
		}
		if ($max > 0 && $count > $max) break;
	}
	$output .= "</ul>\n";

	$feedFile = fopen($path, 'w');
	fwrite($feedFile, $output);
	fclose($feedFile);

}
