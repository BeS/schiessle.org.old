<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all">
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="/augustinus/augustinus.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Björn Schießle - Talks</title>
</head>
  
<body>

<div id="page">

<div id="header">
  <?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">


<h1>Free Software philosophy in the fourth century</h1>

<p>
  <ul>
    <div id="sfb">"Omnis enim res, quae dando non deficit, dum habetur
      et non datur, nondum habetur, quomodo habenda est."
    </div>
  </ul>
  <br />
  (Book I, Chapter 1 "De doctrina christiana" "Corpus Christianorum", "Series
  latina", Vol. 32, p. 6, lines 10-11.  Written 397 AD by Saint
  Augustinus.)
</p>

<p>
  <ul class="augustinus">
    <li>English translation:<br />
      <ul>
	<div id="sf" class="translation">"For if a thing is not diminished by being shared
	  with others, it is not rightly owned if it is only owned and
	  not shared."
	</div>
      </ul>
      (from <code>
	<a href="/web/20070721101357/http://www.ccel.org/a/augustine/doctrine/doctrine.html">http://www.ccel.org/a/augustine/doctrine/doctrine.html</a>
      </code>)
    </li>
  </ul>
</p>
    
<p>
  <ul class="augustinus">
    <li>German translation:<br />
      <ul>
	<div id="sf" class="translation">
	  "Denn jede Sache, die durch Mitteilung an andere nicht
	  verliert, besitzt man nicht, wie man soll, solange man sie
	  nur selber besitzt, ohne sie wieder an andere
	  weiterzugeben."
	</div>
      </ul>
      (from Sigisbert Mitterer, Bibliothek der Kirchenväter, Bd. 49, München 1925)
      A slightly better translation would probably be:<br />
      <ul>
	<div id="sf" class="translation">
	  "Denn jede Sache, die durch Weitergabe an andere nicht
	  verliert, besitzt man nicht, wie man soll, solange man sie
	  nicht an andere weitergibt."
	</div>
      </ul>
    </li>
  </ul>
</p>


<p>
  The context of the quote is the sharing of knowledge through
  teaching and writing. So it almost perfectly fits the philosophy of
  Free Software.
</p>

<h2>About Aurelius Augustinus</h2>

<table>
  <tr>
    <td width="140" align="center" valign="top">
      <a href="img/Augustinus.jpg"> <img src="img/Augustinus_small.jpg" alt="[ Picture of St. Augustinus - Lateranpalace in     Rome, Italy (6th century AD) ]" border="0" /></a> 

    </td>
    <td>
      <p>
	[ to the left: oldest known picture of St. Augustinus; from
	the Lateranpalace in Rome, Italy (6th century AD) ]
      </p>
    
      <p>
	<ul class="augustinus">
	  <li>
	    born 354 in the Roman province town Thagaste (current Algeria)
	  </li>
	  <br />
	  <li>
	    studies of Philosophy, Grammar and Rethorik in Thagaste, Carthago,
	    Rome and Milano
	  </li>
	  <br />
	  <li>
	    386 AD found his way to Christianity
	  </li>
	  <br />
	  <li>
	    387 AD baptized
	  </li>
	  <br />
	  <li>
	    390/391 AD Aurelius Augustinus became priest and shortly after
	    bishop of Hippo Regius (current Algeria)
	  </li>
	  <br />
	  <li>
	    important role in the African church and important factor in the
	    solution of dogmatic quarrels
	  </li>
	  <br />
	  <li>
	    430 AD Augustinus died when the Vandals besieged the town
	  </li>
	</ul>
      </p>
    </td>
  </tr>
</table>
 
<p>
  Due to his role in the dogmatic controversies and his far-spanning
  work, Augustinus has been seen as <em>the</em> theological authority in the
  medieval ages. Nowadays he is held as the most important latin
  founding-father of the church.
</p>

<h2>Many thanks to</h2>

<p>
  <ul class="augustinus">
    <li>
      <b>Steven G. Johnson</b> who pointed this great quote out to us.</li>
    <br />
    <li>
      <b>Dr. Michael Becht</b> (head of faculty, library of the theological
      faculty, University of Freiburg i. Br., Germany) who helped me
      with my research and made the original quote available as
      well as providing all the additional info. He also supplied the
      picture of St. Augustinus from the Lateranpalace in Rome.
    </li>
  </ul>
</p>

<h2>Original author</h2>

<div id="sf">
  <p>Copyright 2000,2001 <a href="mailto:greve@gnu.org">Georg C. F. Greve</a>
    <br />
    Verbatim copying and distribution is permitted
    in any medium, provided this notice is preserved.
  </p>
  <p>
    A copy of the original version can be found at the <a href="https://web.archive.org/web/20070721101357/http://gnuhh.org/work/fsf-europe/augustinus.html">Internet Archive</a>.
  </p>
  
</div>

</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</div>

</body>
</html>
