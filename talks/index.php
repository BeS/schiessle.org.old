<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all">
<meta name="author" content="Björn Schiessle" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="slides.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Björn Schießle - Talks</title>
</head>
  
<body>

<div id="page">

<div id="header">
  <?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>

<div id="content">

  <p class="quote">
    <i>"Omnis enim res, quae dando non deficit, dum habetur et non datur, nondum
      habetur, quomodo habenda est."</i><br />
    ‒ <a href="/augustinus">Aurelius Augustinus</a>
  </p>
  
  <p>
    I give talks about various topics related to the political and
    social change driven by the digitalization of our daily life. This
    includes topics arround Free Software (aka Open Source), Open
    Standards, cloud computing and federated web services. Depending
    on the audience the talks are rather technical or more on a
    philosophical level around technological or ethical principles. If
    this sounds interesting to you,
    please <a href="&#109;&#097;&#105;&#108;&#116;&#111;&#058;&#098;&#106;&#111;&#101;&#114;&#110;&#064;&#115;&#099;&#104;&#105;&#101;&#115;&#115;&#108;&#101;&#046;&#111;&#114;&#103;">contact
    me</a>. I'm happy to speak at your next conference or in-house
    event.
  </p>
  <p>
    In order to reduce the dependency on 3rd party web services I
    decided to host some of my slides directly on this web page. Some of my
    older slides are still available
    at <a href="https://slideshare.net/schiesbn">Slideshare</a>.
</p>

<!-- 2016-09-28 -->
<div class="slides-box">
<a href="open16"><img src="open16/screenshot.jpg" alt="Restore the Internet - Free, Decentralized, Open"></a><br/>
<a href="open16">Open16 Keynote (en)</a>
</div>

<!-- 2016-09-16 -->
<div class="slides-box">
<a href="nextcloudconf16"><img src="nextcloudconf16/screenshot.jpg" alt="Cloud Federation - Past, Present and Future"></a><br/>
<a href="nextcloudconf16">The current state & outlook (en)</a>
</div>

<!-- 2015 -->
<div class="slides-box">
<a href="owncloudconf15"><img src="owncloudconf15/screenshot.jpg" alt="Server Side Encryption in ownCloud and Nextcloud"></a><br/>
<a href="nextcloudconf15">Server Side Encryption 2.0 (en)</a>
</div>

<!-- 2014-10-25 -->
<div class="slides-box">
<a href="GJKarlsruhe"><img src="GJKarlsruhe/screenshot.jpg" alt="Freie Software in Politik und Wirtschaft"></a><br/>
<a href="GJKarlsruhe">Workshop Grüne Jugend BW (de)</a>
</div>

<!-- 2013-08-25 -->
<div class="slides-box">
<a href="froscon13"><img src="froscon13/screenshot.jpg" alt="ownCloud - Meine Daten gehören mir!"></a><br/>
<a href="froscon13">Reclaim control over your data (de)</a>
</div>

<!-- 2010-01-30 -->
<div class="slides-box">
<a href="sofa10"><img src="sofa10/screenshot.jpg" alt="Freie Software - Mehr als nur ein Entwicklungsmodell"></a><br/>
<a href="GJKarlsruhe">Talk at the Stuttgart Open Fair (de)</a>
</div>

<!-- 2009-03-28 -->
<div class="slides-box">
<a href="ccc09"><img src="ccc09/screenshot.jpg" alt="Freie Software und Datenschutz"></a><br/>
<a href="ccc09">Talk at the CCC Stuttgart (de)</a>
</div>

<!-- 2008-10-22 -->
<div class="slides-box">
<a href="systems08"><img src="systems08/screenshot.jpg" alt="Was ist Freie Software"></a><br/>
<a href="systems08">A introduction to Free Software (de)</a>
</div>

<div class="clear"></div>

<p>
Are you are interested in similar talks? Feel free to get in contact
with <a href="&#109;&#097;&#105;&#108;&#116;&#111;&#058;&#098;&#106;&#111;&#101;&#114;&#110;&#064;&#115;&#099;&#104;&#105;&#101;&#115;&#115;&#108;&#101;&#046;&#111;&#114;&#103;">me</a>. I
give talks in English and in German.
</p>

</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</div>

</body>
</html>
