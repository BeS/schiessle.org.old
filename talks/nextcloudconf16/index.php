<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="/talks/slides.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Slides - Cloud Federation</title>
</head>

<body>

<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
<div id="content">

<h1 class="slides">Cloud Federation - Past, Present and Future</h1> 

<p class="center">
<iframe src = "/ViewerJS/#../talks/nextcloudconf16/federation.pdf" width='600' height='450' allowfullscreen webkitallowfullscreen></iframe>
</p>

<div class="flex-list">
  <ul>
    <li>Occasion: Nextcloud Conference 2016</li>
    <li>Location: TU-Berlin (Germany)</li>
    <li>Date: 2016-09-16</li>
    <li>Source: <a href="https://gitlab.com/BeS/presentations/blob/master/Nextcloud/cloud%20federation/Nextcloud%20Conference%202016/federation%20-%20long%20version.odp">LibreOffice Impress</a></li>
  </ul>
</div>

<p>
Abstract: This presentation describes the idea behind cloud federation, shows the current state and discusses future improvements such as extending it to areas beyond files and introducing a global address book.
</p>
<p class="center">
  License: <a href="https://creativecommons.org/licenses/by-sa/3.0/de/deed.en">CC BY-SA 3.0</a>
</p>

</div>

<?php echo file_get_contents("../../footer-cc-by-sa.html") ?>

</body>
</html>
