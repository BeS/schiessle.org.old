<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="/talks/slides.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Slides - Restore the Internet</title>
</head>

<body>

<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
<div id="content">

<h1 class="slides">Restore the Internet - Free, Decentralized, Open</h1> 
  
<p class="center">
<iframe src = "/ViewerJS/#../talks/open16/open16-lowres.pdf" width='600' height='450' allowfullscreen webkitallowfullscreen></iframe>
</p>

<div class="flex-list">
  <ul>
    <li>Occasion: Keynote Open16</li>
    <li>Location: Technopolis (Mechelen/Belgium)</li>
    <li>Date: 2016-09-28</li>
    <li>High Resolution: <a href="https://gitlab.com/BeS/presentations/blob/master/Nextcloud/general/open16%20keynote%20in%20Mechelen%20-%20Why%20Nextcloud%20and%20Cloud%20Federation.pdf">PDF</a>
    <li>Source: <a href="https://gitlab.com/BeS/presentations/blob/master/Nextcloud/general/open16%20keynote%20in%20Mechelen%20-%20Why%20Nextcloud%20and%20Cloud%20Federation.odp">LibreOffice Impress</a></li>
  </ul>
</div>

<p>
Abstract: The presentation starts with a retrospection on why Nextcloud exists and what we want to do different compared to other solutions. The second part covers the main topic, how to restore a free, decentralized and open internet. The problem and possible solutions are illustrated by Nextcloud and Cloud Federation.
</p>
<p class="center">
  License: <a href="https://creativecommons.org/licenses/by-sa/3.0/de/deed.en">CC BY-SA 3.0</a>
</p>

</div>

<?php echo file_get_contents("../../footer-cc-by-sa.html") ?>

</body>
</html>
