<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<link rel="stylesheet" href="/main.css" type="text/css" />
<link rel="stylesheet" href="/about/cv.css" type="text/css" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="meta" type="application/rdf+xml" title="FOAF" href="foaf.rdf" />
<link rel="author" href="https://plus.google.com/110440960198152941904" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>About Björn Schießle</title>
</head>
  
<body class="h-card">
  
<div id="header">
  <?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
<div id="content">

<div id="aboutme">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/short.bio.html") ?>
</div>

<div class="clear"></div>

<p class="quote">
  <i>What you do makes a difference, and you have to decide what kind of
  difference you want to make.</i><br />
  ‒ Jane Goodall
</p>


<h1>Summary</h1>

<div class="inner-content">

<p>
  As a child science kits were always one of my favorite toys. This
  way I discovered already early in my life the world of biology,
  chemistry, electronics and other exiting stuff. I was always driven
  by the childlike curiosity to understand how things work instead of
  just using it. Today I'm still like that. That's probably also the
  reason why my first computer soon lead me down the rabbit hole in
  the world of programming and Free Software (aka Open Source).
</p>
<p>
  During my education and professional career I collected deep
  knowledge about programming and the design and architecture of
  software systems. After I graduated I joined the university as a
  researcher in the area of Cloud Robotics. During this time I learned
  a lot about academic research and writing and enjoyed the challenge
  to solve complex tasks. Today I work at Nextcloud. I develop and
  maintain various parts of a large and complex Free Software solution
  which provides universal access to data stored in the cloud without
  losing control or privacy. Being involved in the Free Software
  movement for over 20 years lead to a deep knowledge about Free
  Software, Open Standards and software licenses. Today I'm fascinated
  about how technology shapes our society and how society can shape
  technology. Free Software is not just a black-box people can use,
  but it enables everyone to understand it and adapt it to their
  needs. In a world driven by technology this are important rights for
  individuals, organisations and big enterprises. I love to discuss
  this new possibilities, attend related trade shows and think about
  how Free Software can enable a business to grow.
</p>
<p>
  If I look back at the end of a working day I want to have the
  feeling that I did something useful for my company, our customers
  and for the society as a whole. That's why I love working with
  people from all over the world in a open and collaborative
  environment, driven by Free Software and Open Standards.
</p>

</div>

 <h1>Education</h1>

<div class="inner-content">
 
   <div class="company">University of Stuttgart</div>
   <div class="period">2010</div>
   <br />
   <div class="role">Diplom (equivalent to M.Sc.), Computer Science</div>

</div>
   
 <h1>Skills</h1>

<div class="inner-content">
 
 <ul>
   <li>Free Software / Open Source</li>
   <li>Software Licenses</li>
   <li>Software Engineering</li>
   <li>Cloud Computing</li>
   <li>API Design</li>
   <li>Distributed and Federated Systems</li>
   <li>Web Applications</li>
   <li>GNU/Linux</li>
   <li>PHP</li>
   <li>JavaScript</li>
   <li>Version Control (Git, SVN, CVS)</li>
   <li>C/C++</li>
   <li>Qt</li>
   <li>Python</li>
 </ul>

</div>
 
  <h1>Experience</h1>
  
<div class="inner-content">

   <div class="company">Nextcloud GmbH</div>
   <div class="period">6/2016 - Present</div>
   <br />
   <div class="role">Co-Founder & Senior Software Engineer</div>
   <br />
   <div class="description">
     Engaging with a globally distributed community, encourage them
     and help them to get involved is a vital part of my daily work in
     a company build around a healthy and diverse Free Software (aka
     Open Source) project.  As a senior software engineer I'm working
     on the server component of Nextcloud using object oriented PHP,
     JavaScript, HTML and CSS. One of my main responsibilities include
     moving the concept of Cloud Federation to the next level, a
     technology which enables a complete new way to collaborate in a
     free, distributed and open way without losing control over
     personal and sensitive data. Beside that I serve as a Free
     Software and licensing expert at Nextcloud.
   </div>

   <br /><br />
  
   <div class="company">Free Software Foundation Europe (FSFE)</div>
   <div class="period">9/2015 - Present</div>
   <br />
   <div class="role">Deputy Coordinator Germany (voluntary)</div>
   <br />
   <div class="description">
     Coordinate the activities of the German country team at FSFE to promote
     Free Software in order to enable people to control the technology they use.
   </div>

   <br /><br />
   
   <div class="company">Free Software Foundation Europe (FSFE)</div>
   <div class="period">2006 - Present</div>
   <br />
   <div class="role">Member of the German Country Team and the European Core Team (voluntary)</div>
   <br />
   <div class="description">
   Educate people about the political, social and economical
   implications of Free Software (Open Source) and Open Standards. I
   give talks at various trade shows and other events about Free
   Software and related topics. Occasionally I talk to political
   parties, their youth organizations and companies to advise them
   about Free Software and licensing. Since 2016 I'm a member of the
   board.
   </div>

   <br /><br />
   
   <div class="company">ownCloud Inc.</div>
   <div class="period">6/2012 - 5/2016</div>
   <br />
   <div class="role">Senior Software Engineer</div>
   <br />
   <div class="description">
     My responsibilities include maintaining and expanding the core of ownCloud
     along with working with a globally distributed community of contributors
     using object oriented PHP, JavaScript, HTML and CSS. I'm the maintainer of
     various core modules including server-side encryption, deleted files,
     versioning and sharing.
   </div>

   <br /><br />

   <div class="company">University of Stuttgart</div>
   <div class="period">2010 -5/2012</div>
   <br />
   <div class="role">Researcher</div>
   <br />
   <div class="description">
     Working at the RoboEarth project which is part of the Cognitive Systems and
     Robotics Initiative from the European Union Seventh Framework Programme
     FP7/2007-2013. Developing a distributed web-based platform which enables robots
     to share reusable knowledge about actions, objects and environments.
   </div>

</div>


 <h1>Publications</h1>

<div class="inner-content">

<p>
  Werkzeugkasten Freie Software - Einführung in die Grundlagen
  Freier Software zusammen mit einer Sammlung von sinnvoller
  Software, mit der Unterricht in einem digitalen Zeitalter
  gestaltet werden kann. Autoren: Volker Grassmuck, Daniel
  Rohde-Kage, Björn Schießle, Stefan Schober, Sebastian Seitz,
  Wolf-Dieter Zimmermann (2016)
</p>

<p>
  Defensive Publication: Cloud file syncing encryption system. Hugo
  Roy and Björn Schießle (2014)
</p>

<p>
  Freie Software – Ein wachsender Faktor in Wirtschaft und
  Gesellschaft. Erschienen im Handbuch der Unternehmensberatung, 2013, Band 2,
  Kennzahl 3410, Erich Schmidt Verlag
</p>

<p>
  Creating and using roboearth object models. CRA, pages 3549–3550,
  2012. Daniel Di Marco, Andreas Koch, Oliver Zweigle, Kai Häussermann, Björn
  Schiessle, Paul Levi, Dorian Gálvez-López, Luis Riazuelo, Javier Civera,
  J. M. M. Montiel, Moritz Tenorth, Alexander Clifford Perzylo, Markus Waibel,
  and René van de Molengraft
</p>

<p>
  Server-sided automatic map transformation in roboearth. AMS, pages 203–216,
  2012. Alexander Clifford Perzylo, Björn Schießle, Kai Häussermann, Oliver
  Zweigle, Paul Levi, and Alois Knoll.
</p>

<p>
  RoboEarth - A World Wide Web for Robots. IEEE Robotics and Automation
  Magazine (Special Issue Towards a WWW for Robots), 2011. Accepted - Waibel,
  M., Beetz, M., D’Andrea, R., Janssen, R., Tenorth, M., Civera, J., Elfring,
  J., Galvez-Lopez, D., Häussermann, K., Montiel, J.M.M., Perzylo, A.,
  Schießle, B., Zweigle, O., and van de Molengraft, R. (2011)
</p>

<p>
  Using Situation Analysis Techniques with Bayesian Networks for Recognizing
  Hardware Errors on Mobile Robots - Oliver Zweigle, Kai Häussermann, Björn
  Schiessle and Paul Levi (ICRA 2011)
</p>

</div>

<h1>Affiliation</h1>

<div class="inner-content">

  <p id="affiliation">

  <a href="https://www.fsfe.org"><img class="affiliation-img" src="/img/affiliated/fsfe.png" alt="FSFE" title="Free Software Foundation Europe (volunteer)" /></a>

  <a href="http://www.fiff.de"><img class="affiliation-img" src="/img/affiliated/fiff.jpg" alt="FIfF" title="Forum InformatikerInnen für Frieden und gesellschaftliche Verantwortung - Computer Scientists for Peace and Social Responsibility (sustaining member)" /></a>

  <a href="https://www.edri.org"><img class="affiliation-img" src="/img/affiliated/edri.png" alt="EDRi" title="European Digital Rights (sustaining member)" /></a>

  <a href="https://digitalegesellschaft.de/"><img class="affiliation-img" src="/img/affiliated/digiges.jpg" alt="Digitale Gesellschaft e.V." title="German association committed to civil rights and consumer protection in terms of internet policy (sustaining member)" /></a>
</p>
  
</div>

</div>

<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/footer.html") ?>

</body>
</html>
