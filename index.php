<!-- -*- mode: html; -*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="all" />
<meta name="author" content="Björn Schießle" />
<meta name="description" content="Bjoern Schiessle's personal homepage" />
<meta name="microid" content="mailto+http:sha1:b558840b6b12dfa4f534b367b51c7b9edc5c3ea2" />
<link rel="openid.server" href="http://openid.claimid.com/server" />
<link rel="openid.delegate" href="http://openid.claimid.com/schiesbn" />
<link href="https://plus.google.com/110440960198152941904" rel="publisher" />
<link rel="stylesheet" href="main.css" type="text/css" />
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="pavatar" href="http://www.schiessle.org/pics/hackergotchi_80x80.png" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link rel="author" href="https://plus.google.com/110440960198152941904" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title>Björn Schießle's Personal Homepage</title>
</head>
  
<body class="h-card">
  
<div id="header">
<?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/menu.html") ?>
</div>
<div id="content">

<div id="aboutme">
  <?php echo file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/short.bio.html") ?>
  <a href="https://io.schiessle.org/bes" rel="me" class="sn-button u-url" title="GNU Social"><img src="img/network/gnusocial.png" alt="Join me on GnuSocial"/></a>
  <a href="https://diasp.eu/u/bjoern" rel="me" class="sn-button u-url" title="Diaspora"><img src="img/network/diaspora.png" alt="Join me on Diaspora"/></a>
  <a href="https://twitter.com/schiessle" rel="me" class="sn-button u-url" title="Twitter"><img src="img/network/twitter.png" alt="Follow me on Twitter"/></a>
  <a href="https://gitlab.com/u/BeS" rel="me" class="sn-button u-url"
  title="Git"><img src="img/network/git.png" alt="Git Repository"/></a>
  <a href="socialnetworks.php" title="More…">…</a>

</div>

<div class="clear"></div>

<div class="alignRight">
<p>
<b>E-Mail</b><br/><br/>

<img src="/img/contact/email.png" alt="Email Address" title="email private"/> <i><a rel="me" class="email u-email" href="&#109;&#097;&#105;&#108;&#116;&#111;&#058;&#098;&#106;&#111;&#101;&#114;&#110;&#064;&#115;&#099;&#104;&#105;&#101;&#115;&#115;&#108;&#101;&#046;&#111;&#114;&#103;" title="my personal email address">&#098;&#106;&#111;&#101;&#114;&#110;&#064;&#115;&#099;&#104;&#105;&#101;&#115;&#115;&#108;&#101;&#046;&#111;&#114;&#103;</a></i><br/>
<img src="/img/contact/email.png" alt="Email Address" title="email fsfe"/> <i><a rel="me" class="email u-email" href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;" title="for FSFE related topics">&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x40;&#x66;&#x73;&#x66;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a></i><br/>
<img src="/img/contact/email.png" alt="Email Address" title="email nextcloud"/> <i><a rel="me" class="email u-email" href="&#x6D;&#x61;&#x69;&#x6C;&#x74;&#x6F;&#x3A;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x6E;&#x65;&#x78;&#x74;&#x63;&#x6C;&#x6F;&#x75;&#x64;&#x2E;&#x63;&#x6F;&#x6D;" title="for Nextcloud related topics">&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x6E;&#x65;&#x78;&#x74;&#x63;&#x6C;&#x6F;&#x75;&#x64;&#x2E;&#x63;&#x6F;&#x6D;</a></i><br/>

<div class="note">
(If you send me a attachments,<br/>
please use formats <a href="openstandards/">everyone can read!</a>)
</div>
</p>

<p>
<b>Instant Messaging</b><br/><br/>

<img src="/img/contact/xmpp.png" alt="XMPP ID" title="XMPP (Jabber) with OTR and OMEMO support"/> <i><a rel="me" class="url" title="First add me as a contact and then start messaging, anonymous messages are blocked to avoid spam issues." href="&#x78;&#x6D;&#x70;&#x70;&#x3A;&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;">&#x62;&#x6A;&#x6F;&#x65;&#x72;&#x6E;&#x40;&#x73;&#x63;&#x68;&#x69;&#x65;&#x73;&#x73;&#x6C;&#x65;&#x2E;&#x6F;&#x72;&#x67;</a></i><br/>

<img src="/img/contact/chat.png" alt="IRC Chat"/> <i>BeS@irc.freenode.net</i>
<a href="irc://irc.freenode.net:6667/fsfe">#fsfe</a> 
<a href="irc://irc.freenode.net:6667/nextcloud">#nextcloud</a>
<a href="irc://irc.freenode.net:6667/nextcloud-dev">#nextcloud-dev</a>

<div class="note">
  Please consider encryption for<br/>
  secure and private communication. <a href="privacy/">More...</a>
</div>

</p>

</div>

<br/><br/><br/>

<div id="feeds">
<div id="left_column">
<div class="headline"><a href="http://blog.schiessle.org">Weblog</a> <a href="http://blog.schiessle.org/feed/" class="button"><img src="/img/rss.png" alt="Weblog RSS Feed" title="RSS Feed"/></a></div>
<?php echo file_get_contents("feeds/blog.html"); ?>
<div class="more"><a href="https://blog.schiessle.org">More...</a></div>
</div>
<div id="right_column">
<div class="headline"><a href="https://io.schiessle.org/bes">&#956;-Blog</a> <a href="https://io.schiessle.org/api/statuses/user_timeline/1.rss" class="button"><img src="/img/rss.png" alt="Micro-Blog RSS Feed" title="RSS Feed"/></a></div>
<?php echo file_get_contents("feeds/gnusocial.html"); ?>
<div class="more"><a href="https://io.schiessle.org/bes">More...</a></div>
</div>
</div>

</div>

<?php echo file_get_contents("footer.html") ?>

</body>
</html>
